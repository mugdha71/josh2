package com.example.lenovo.josh2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.josh2.UI.About;
import com.example.lenovo.josh2.UI.Support;
import com.example.lenovo.josh2.topApps.Bikroy;
import com.example.lenovo.josh2.topApps.Chaldal;
import com.example.lenovo.josh2.topApps.Daraz;
import com.example.lenovo.josh2.topApps.Food;
import com.example.lenovo.josh2.topApps.MobileRecharge;
import com.example.lenovo.josh2.topApps.Offer;
import com.example.lenovo.josh2.topApps.Pickaboo;
import com.example.lenovo.josh2.topApps.Rokomari;
import com.example.lenovo.josh2.topApps.Uber;
import com.example.lenovo.josh2.top_educational_portal.KhanAcademy;
import com.example.lenovo.josh2.top_educational_portal.TenMinuteSchool;
import com.example.lenovo.josh2.top_educational_portal.Vocabulary;
import com.example.lenovo.josh2.top_job_portals.BdJobs;
import com.example.lenovo.josh2.top_job_portals.Chakri;
import com.example.lenovo.josh2.top_job_portals.Kormo;
import com.example.lenovo.josh2.top_newspapers.DailyIttefaq;
import com.example.lenovo.josh2.top_newspapers.DailyStar;
import com.example.lenovo.josh2.top_newspapers.ProthomAlo;
import com.example.lenovo.josh2.top_online_portals.BdNews24;
import com.example.lenovo.josh2.top_online_portals.EgiyeCholo;
import com.example.lenovo.josh2.top_online_portals.Priyo;
import com.example.lenovo.josh2.top_sports_portal.Cricbuzz;
import com.example.lenovo.josh2.top_sports_portal.Espn;
import com.example.lenovo.josh2.top_sports_portal.YahooSports;

public class Navigation_New extends AppCompatActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    ImageView mobileRechargeImage, foodImage, offerImage, rokomariImage, darazImage, pickabooImage, uberImage, bikroyImage, chaldalImage;
    TextView mobileRechargeText, foodText, offerText, rokomariText, darazText, pickabooText, uberText, bikroy_text, chaldalText;

    ImageView prothomAloImage, dailyStarImage, dailyIttefaqImage;
    TextView prothomAloText, dailyStarText, dailyIttefaqText;

    ImageView priyoImage, egiyeCholoImage, bdNews24Image;
    TextView priyoText, egiyeCholoText, bdNews24Text;

    ImageView espnImage, cricbuzzImage, yahooSportsImage;
    TextView espnText, cricbuzzText, yahooSportsText;

    ImageView bdJobsImage, kormoImage, chakriImage;
    TextView bdJobsText, kormoText, chakriText;

    ImageView tenMinutesSchoolImage, khanAcademyImage, vocabularyImage;
    TextView tenMinutesSchoolText, khanAcademyText, vocabularyText;

    MenuItem support, menuUsername;

    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_test);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        Intent intent = getIntent();
        String name = intent.getStringExtra("email");

        menuUsername = findViewById(R.id.username);

        initialize();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.mobile_recharge_image:
            case R.id.mobile_recharge_text:
                startActivity(new Intent(Navigation_New.this,MobileRecharge.class));
                break;

            case R.id.food_image:
            case R.id.food_text:
                startActivity(new Intent(Navigation_New.this,Food.class));
                break;

            case R.id.offer_image:
            case R.id.offer_text:
                startActivity(new Intent(Navigation_New.this,Offer.class));
                break;

            case R.id.rokomari_image:
            case R.id.rokomari_text:
                startActivity(new Intent(Navigation_New.this,Rokomari.class));
                break;

            case R.id.daraz_image:
            case R.id.daraz_text:
                startActivity(new Intent(Navigation_New.this,Daraz.class));
                break;

            case R.id.pickaboo_image:
            case R.id.pickaboo_text:
                startActivity(new Intent(Navigation_New.this,Pickaboo.class));
                break;

            case R.id.uber_image:
            case R.id.uber_text:
                startActivity(new Intent(Navigation_New.this,Uber.class));
                break;

            case R.id.bikroy_image:
            case R.id.bikroy_text:
                startActivity(new Intent(Navigation_New.this,Bikroy.class));
                break;

            case R.id.chaldal_image:
            case R.id.chaldal_text:
                startActivity(new Intent(Navigation_New.this,Chaldal.class));
                break;

            case R.id.prothom_alo_image:
            case R.id.prothom_alo_text:
                startActivity(new Intent(Navigation_New.this,ProthomAlo.class));
                break;

            case R.id.daily_star_image:
            case R.id.daily_star_text:
                startActivity(new Intent(Navigation_New.this,DailyStar.class));
                break;

            case R.id.daily_ittefaq_image:
            case R.id.daily_ittefaq_text:
                startActivity(new Intent(Navigation_New.this,DailyIttefaq.class));
                break;

            case R.id.priyo_image:
            case R.id.priyo_text:
                startActivity(new Intent(Navigation_New.this,Priyo.class));
                break;

            case R.id.egiye_cholo_image:
            case R.id.egiye_cholo_text:
                startActivity(new Intent(Navigation_New.this,EgiyeCholo.class));
                break;

            case R.id.bd_news24_image:
            case R.id.bd_news24_text:
                startActivity(new Intent(Navigation_New.this,BdNews24.class));
                break;

            case R.id.espn_image:
            case R.id.espn_text:
                startActivity(new Intent(Navigation_New.this,Espn.class));
                break;

            case R.id.cricbuzz_image:
            case R.id.cricbuzz_text:
                startActivity(new Intent(Navigation_New.this,Cricbuzz.class));
                break;

            case R.id.yahoo_sports_image:
            case R.id.yahoo_sports_text:
                startActivity(new Intent(Navigation_New.this,YahooSports.class));
                break;

            case R.id.bd_jobs_image:
            case R.id.bd_jobs_text:
                startActivity(new Intent(Navigation_New.this,BdJobs.class));
                break;

            case R.id.kormo_image:
            case R.id.kormo_text:
                startActivity(new Intent(Navigation_New.this,Kormo.class));
                break;

            case R.id.chakri_image:
            case R.id.chakri_text:
                startActivity(new Intent(Navigation_New.this,Chakri.class));
                break;

            case R.id.ten_minute_school_image:
            case R.id.ten_minute_school_text:
                startActivity(new Intent(Navigation_New.this,TenMinuteSchool.class));
                break;

            case R.id.khan_academy_image:
            case R.id.khan_academy_text:
                startActivity(new Intent(Navigation_New.this,KhanAcademy.class));
                break;

            case R.id.vocabulary_image:
            case R.id.vocabulary_text:
                startActivity(new Intent(Navigation_New.this,Vocabulary.class));
                break;

            case R.id.support:
                startActivity(new Intent(Navigation_New.this,Support.class));

        }
    }

    private void initialize(){
        Drawable d=getResources().getDrawable(R.drawable.gradientblue);

        if(getSupportActionBar() != null)
            getSupportActionBar().setBackgroundDrawable(d);

        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileRechargeImage = findViewById(R.id.mobile_recharge_image);
        mobileRechargeImage.setOnClickListener(this);

        mobileRechargeText = findViewById(R.id.mobile_recharge_text);
        mobileRechargeText.setOnClickListener(this);

        foodImage = findViewById(R.id.food_image);
        foodImage.setOnClickListener(this);

        foodText = findViewById(R.id.food_text);
        foodText.setOnClickListener(this);

        offerImage = findViewById(R.id.offer_image);
        offerImage.setOnClickListener(this);

        offerText = findViewById(R.id.offer_text);
        offerText.setOnClickListener(this);

        rokomariImage = findViewById(R.id.rokomari_image);
        rokomariImage.setOnClickListener(this);

        rokomariText = findViewById(R.id.rokomari_text);
        rokomariText.setOnClickListener(this);

        darazImage = findViewById(R.id.daraz_image);
        darazImage.setOnClickListener(this);

        darazText = findViewById(R.id.daraz_text);
        darazText.setOnClickListener(this);

        pickabooImage = findViewById(R.id.pickaboo_image);
        pickabooImage.setOnClickListener(this);

        pickabooText = findViewById(R.id.pickaboo_text);
        pickabooText.setOnClickListener(this);

        uberImage = findViewById(R.id.uber_image);
        uberImage.setOnClickListener(this);

        uberText = findViewById(R.id.uber_text);
        uberText.setOnClickListener(this);

        bikroyImage = findViewById(R.id.bikroy_image);
        bikroyImage.setOnClickListener(this);

        bikroy_text = findViewById(R.id.bikroy_text);
        bikroy_text.setOnClickListener(this);

        chaldalImage = findViewById(R.id.chaldal_image);
        chaldalImage.setOnClickListener(this);

        chaldalText = findViewById(R.id.chaldal_text);
        chaldalText.setOnClickListener(this);

        prothomAloImage = findViewById(R.id.prothom_alo_image);
        prothomAloImage.setOnClickListener(this);

        prothomAloText = findViewById(R.id.prothom_alo_text);
        prothomAloText.setOnClickListener(this);

        dailyStarImage = findViewById(R.id.daily_star_image);
        dailyStarImage.setOnClickListener(this);

        dailyStarText = findViewById(R.id.daily_star_text);
        dailyStarText.setOnClickListener(this);

        dailyIttefaqImage = findViewById(R.id.daily_ittefaq_image);
        dailyIttefaqImage.setOnClickListener(this);

        dailyIttefaqText = findViewById(R.id.daily_ittefaq_text);
        dailyIttefaqText.setOnClickListener(this);

        priyoImage = findViewById(R.id.priyo_image);
        priyoImage.setOnClickListener(this);

        priyoText = findViewById(R.id.priyo_text);
        priyoText.setOnClickListener(this);

        egiyeCholoImage = findViewById(R.id.egiye_cholo_image);
        egiyeCholoImage.setOnClickListener(this);

        egiyeCholoText = findViewById(R.id.egiye_cholo_text);
        egiyeCholoText.setOnClickListener(this);

        bdNews24Image = findViewById(R.id.bd_news24_image);
        bdNews24Image.setOnClickListener(this);

        bdNews24Text = findViewById(R.id.bd_news24_text);
        bdNews24Text.setOnClickListener(this);

        espnImage = findViewById(R.id.espn_image);
        espnImage.setOnClickListener(this);

        espnText = findViewById(R.id.espn_text);
        espnText.setOnClickListener(this);

        cricbuzzImage = findViewById(R.id.cricbuzz_image);
        cricbuzzImage.setOnClickListener(this);

        cricbuzzText = findViewById(R.id.cricbuzz_text);
        cricbuzzText.setOnClickListener(this);

        yahooSportsImage = findViewById(R.id.yahoo_sports_image);
        yahooSportsImage.setOnClickListener(this);

        yahooSportsText = findViewById(R.id.yahoo_sports_text);
        yahooSportsText.setOnClickListener(this);

        bdJobsImage = findViewById(R.id.bd_jobs_image);
        bdJobsImage.setOnClickListener(this);

        bdJobsText = findViewById(R.id.bd_jobs_text);
        bdJobsText.setOnClickListener(this);

        kormoImage = findViewById(R.id.kormo_image);
        kormoImage.setOnClickListener(this);

        kormoText = findViewById(R.id.kormo_text);
        kormoText.setOnClickListener(this);

        chakriImage = findViewById(R.id.chakri_image);
        chakriImage.setOnClickListener(this);

        chakriText = findViewById(R.id.chakri_text);
        chakriText.setOnClickListener(this);

        tenMinutesSchoolImage = findViewById(R.id.ten_minute_school_image);
        tenMinutesSchoolImage.setOnClickListener(this);

        tenMinutesSchoolText = findViewById(R.id.ten_minute_school_text);
        tenMinutesSchoolText.setOnClickListener(this);

        khanAcademyImage = findViewById(R.id.khan_academy_image);
        khanAcademyImage.setOnClickListener(this);

        khanAcademyText = findViewById(R.id.khan_academy_text);
        khanAcademyText.setOnClickListener(this);

        vocabularyImage = findViewById(R.id.vocabulary_image);
        vocabularyImage.setOnClickListener(this);

        vocabularyText = findViewById(R.id.vocabulary_text);
        vocabularyText.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.username:
                return true;
            case R.id.home:
                return true;
            case R.id.refer_and_win:
                return true;
            case R.id.faq:
                return true;
            case R.id.support:
                startActivity(new Intent(Navigation_New.this,Support.class));
                return true;
            case R.id.about_josh_app:
                startActivity(new Intent(Navigation_New.this,About.class));
                return true;
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
