package com.example.lenovo.josh2.topApps;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.lenovo.josh2.R;

public class Food extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_apps_food);

        Drawable d=getResources().getDrawable(R.drawable.gradientblue);

        if(getSupportActionBar() != null){
            getSupportActionBar().setBackgroundDrawable(d);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Food");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return  true;
    }
}
