package com.example.lenovo.josh2.UI;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.josh2.Navigation_New;
import com.example.lenovo.josh2.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class Registration extends AppCompatActivity {

    private EditText username, password;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        mAuth = FirebaseAuth.getInstance();

        username = findViewById(R.id.register_username);
        password = findViewById(R.id.register_password);

        progressBar = findViewById(R.id.registration_progressbar);
        progressBar.setVisibility(View.INVISIBLE);

        findViewById(R.id.go_to_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registration.this, Login.class));
            }
        });

        findViewById(R.id.register_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    public void register(){
        final String user = username.getText().toString().trim();
        String pass = password.getText().toString().trim();

        boolean error = false;
        if(user.isEmpty()){
            username.setError("Field Empty");
            username.requestFocus();
            return;
        }

        if(!Patterns.PHONE.matcher(user).matches() || user.length()!=11){
            username.setError("Provide correct phone number!");
            username.requestFocus();
            return;
        }
        if(pass.isEmpty()){
            password.setError("Field Empty");
            password.requestFocus();
            return;
        }

        if(pass.length()<6){
            password.setError("Password must be atleast 6 characters");
            password.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        if(error){
            progressBar.setVisibility(View.INVISIBLE);
        }
        mAuth.createUserWithEmailAndPassword(user,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.INVISIBLE);
                if(task.isSuccessful()){
                    Toast.makeText(Registration.this, "User Registration Successful", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Registration.this,Navigation_New.class);
                    i.putExtra("email", user);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }else {
                    if(task.getException() instanceof FirebaseAuthUserCollisionException){
                        Toast.makeText(Registration.this, "Email already registered!", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(Registration.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
