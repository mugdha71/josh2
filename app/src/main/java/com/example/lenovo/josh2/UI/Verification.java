package com.example.lenovo.josh2.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.lenovo.josh2.MainActivity;
import com.example.lenovo.josh2.R;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class Verification extends AppCompatActivity {

    EditText phoneNumber, signIn;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        findViewById(R.id.send_verification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificationCode();
            }
        });

        findViewById(R.id.check_verification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        phoneNumber = findViewById(R.id.phone_number);
        signIn = findViewById(R.id.verification_code);
        mAuth = FirebaseAuth.getInstance();
    }
    void gotoMain(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    private void sendVerificationCode() {
    {
            String phone = phoneNumber.getText().toString().trim();
            if(phone.isEmpty()) {
                phoneNumber.setError("Empty Field");
                phoneNumber.requestFocus();
                return;
            }

            if(phone.length()!=11){
                phoneNumber.setError("Number must be 11 characters long");
            }
            PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 60, TimeUnit.SECONDS, this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                    //TODO
                    gotoMain();
                }

                @Override
                public void onVerificationFailed(FirebaseException e) {
                    //TODO
                    gotoMain();
                }
            });
        }
    }
}
