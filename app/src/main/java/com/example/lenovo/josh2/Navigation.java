package com.example.lenovo.josh2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.example.lenovo.josh2.topApps.Chaldal;
import com.example.lenovo.josh2.topApps.Daraz;
import com.example.lenovo.josh2.topApps.Pickaboo;
import com.example.lenovo.josh2.topApps.Rokomari;
import com.example.lenovo.josh2.topApps.Sheba;
import com.example.lenovo.josh2.topApps.Uber;

public class Navigation extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private LinearLayout darazButton;
    private LinearLayout rokomariButton;
    private LinearLayout pickabooButton;
    private LinearLayout uberButton;
    private LinearLayout shebaButton;
    private LinearLayout chaldalButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        Drawable d=getResources().getDrawable(R.drawable.gradientblue);
        getSupportActionBar().setBackgroundDrawable(d);

        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        darazButton = findViewById(R.id.daraz_button);
        rokomariButton = findViewById(R.id.rokomari_button);
        pickabooButton = findViewById(R.id.pickaboo_button);
        uberButton = findViewById(R.id.uber_button);
        shebaButton = findViewById(R.id.sheba_button);
        chaldalButton = findViewById(R.id.chaldal_button);

        darazButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this,Daraz.class);
                startActivity(intent);
            }
        });

        rokomariButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this,Rokomari.class);
                startActivity(intent);
            }
        });

        pickabooButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this,Pickaboo.class);
                startActivity(intent);
            }
        });

        uberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this,Uber.class);
                startActivity(intent);
            }
        });

        shebaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this,Sheba.class);
                startActivity(intent);
            }
        });

        chaldalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this,Chaldal.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
