package com.example.lenovo.josh2.topApps;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.lenovo.josh2.R;

public class Daraz extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daraz);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Drawable d=getResources().getDrawable(R.drawable.gradientblue);
        getSupportActionBar().setBackgroundDrawable(d);

        setTitle("Daraz");
        webView = findViewById(R.id.webview_daraz);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://www.daraz.com.bd/");
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return  true;
    }

        @Override
    public void onBackPressed() {

        if(webView.canGoBack()){
            webView.goBack();
        }else {
            super.onBackPressed();
        }
    }
}
