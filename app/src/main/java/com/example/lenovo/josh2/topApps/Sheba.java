package com.example.lenovo.josh2.topApps;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.lenovo.josh2.R;

public class Sheba extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sheba);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Drawable d=getResources().getDrawable(R.drawable.gradientblue);
        getSupportActionBar().setBackgroundDrawable(d);

        setTitle("Sheba");
        webView = findViewById(R.id.webview_sheba);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://www.sheba.xyz");
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return  true;
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()){
            webView.goBack();
        }else {
            super.onBackPressed();
        }
    }
}
