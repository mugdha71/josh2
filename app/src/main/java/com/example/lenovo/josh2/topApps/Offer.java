package com.example.lenovo.josh2.topApps;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.lenovo.josh2.R;

public class Offer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_apps_offer);

        Drawable d=getResources().getDrawable(R.drawable.gradientblue);

        if(getSupportActionBar() != null){
            getSupportActionBar().setBackgroundDrawable(d);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Offer");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return  true;
    }
}
